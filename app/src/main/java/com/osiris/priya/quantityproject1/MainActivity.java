package com.osiris.priya.quantityproject1;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {
    private int quantity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TextView tv = new TextView(this);
//        tv.setText("Hi Trying textview directly in setContentView");
//        tv.setGravity(0);
//        tv.setTextColor(Color.parseColor("#e6e6fa"));
//

        setContentView(R.layout.activity_main);
//        setContentView(tv);
    }

    /*
    *  This method is going to increase quantity by 1
    */
    public void increment(View view) {


            if (quantity == 100) {
                /*
            * Show user error message when they are trying to order more than 100 coffees
            */
                Toast.makeText(this, "You cannot order more than 100 coffees", Toast.LENGTH_SHORT).show();
                // Return the user to beginning
                return;
            }
                quantity++;
                Log.v("Log for < 99" , "Value "+quantity);
                displayQuantity(quantity);
    }
    /*
*  This method is going to decrease quantity by 1
*/
    public void decrement(View view) {

        if (quantity == 1) {
            /*
            * Show user error message when they are trying to order less than 1 coffee
            */
            Toast.makeText(this,"You cannot order less than 1 coffee",Toast.LENGTH_SHORT).show();
            return;
        }
            quantity--;
            Log.v("Log for >1" , "Value "+quantity);
            displayQuantity(quantity);
    }

    /**
     * Calculates the price of the order.
     * <p/>
     * //     * @param quantity is the number of cups of coffee ordered
     *
     * @param pricePerCup is price of each coffee cup
     *                    The above line is complaining as there are no parameters for calculatePrice() which goes if we add
     *                    quantity as in calculatePrice(int quantity) remvoes error
     * @return Returning total price of orders
     */
    private int calculatePrice(int pricePerCup,boolean isCheckedWhippedCream,boolean isCheckedChoc) {
        int pricePerWhippedCream = 1;
        int pricePerChocolate = 2;
        int totalPrice =0;
        if (isCheckedWhippedCream) {
            totalPrice = totalPrice + pricePerWhippedCream;
        }
//        Log.v("First Step","Value "+totalPrice);

        if(isCheckedChoc){
            totalPrice = totalPrice + pricePerChocolate;
        }
//        Log.v("Second Step","Value "+totalPrice);
        totalPrice = totalPrice + quantity * pricePerCup;
        return totalPrice;
    }

    /*
    * This method displays the complete order summary to input checkstate of whipped cream and Chocolate
    */
    private String createOrderSummary(String nameOfCustomer, int totalPrice, boolean hasWhippedCream, boolean hasChocolate) {
        String printMessage = getString(R.string.customername,nameOfCustomer);
        printMessage += "\nAdd Whipped cream? " + hasWhippedCream;
        printMessage += "\nAdd Chocolate? " + hasChocolate;
        printMessage += "\nQuantity : " + quantity;
        printMessage += "\nTotal : $" + totalPrice;
        printMessage += "\n"+getString(R.string.thankyou); //getResources() is optional
        return  printMessage;
    }


    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
//        display(quantity);
//        displayPrice(quantity * 5);
        /*
        *  Determine price of each cup based on selected checkbox
        */
        int pricePerCup = 4;
        CheckBox checkBox1 = (CheckBox) findViewById(R.id.whippedCream);
        CheckBox checkBox2 = (CheckBox) findViewById(R.id.chocolate);
        EditText editText = (EditText) findViewById(R.id.nameEditText);
        boolean isCheckedWhippedCream = checkBox1.isChecked();
        boolean isCheckedChoc = checkBox2.isChecked();
        String name = editText.getText().toString();
        int totalPrice = calculatePrice(pricePerCup,isCheckedWhippedCream,isCheckedChoc);
//        displayMessage(createOrderSummary(name,totalPrice, isCheckedWhippedCream, isCheckedChoc));
        String emailMessage = createOrderSummary(name,totalPrice, isCheckedWhippedCream, isCheckedChoc);

        /*
        *   Add intent to send email about order summary
        */
        Intent intent1 = new Intent(Intent.ACTION_SENDTO);
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sp = new SimpleDateFormat("yyyy/MM/dd");
        intent1.setData(Uri.parse("mailto:"));
        intent1.putExtra(Intent.EXTRA_EMAIL,"priyav.999@gmail.com");
        intent1.putExtra(Intent.EXTRA_SUBJECT,"order summary as of DATE "+ sp.format(date));
        intent1.putExtra(Intent.EXTRA_TEXT,emailMessage);
        if (intent1.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent1);
        }
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int num) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText(""+num);
    }

    /**
     * /*
     * Implementing displayMessage to display string messages
     */

//    private void displayMessage(String message) {
//        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
//        orderSummaryTextView.setText(message);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

 /*
         *  Sample intent code to open Google maps with given coordinates.
        */
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse("geo:47.6, -122.3"));
//        if(intent.resolveActivity(getPackageManager()) != null){
//            startActivity(intent);
